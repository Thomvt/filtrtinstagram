package com.example.myapplication;


import android.content.Context;

import com.example.myapplication.insta.IF1977Filter;
import com.example.myapplication.insta.IFAden;
import com.example.myapplication.insta.IFAmaroFilter;
import com.example.myapplication.insta.IFBrannanFilter;
import com.example.myapplication.insta.IFEarlybirdFilter;
import com.example.myapplication.insta.IFHefeFilter;
import com.example.myapplication.insta.IFHudsonFilter;
import com.example.myapplication.insta.IFImageFilter;
import com.example.myapplication.insta.IFInkwellFilter;
import com.example.myapplication.insta.IFLomoFilter;
import com.example.myapplication.insta.IFLordKelvinFilter;
import com.example.myapplication.insta.IFLudwig;
import com.example.myapplication.insta.IFNashvilleFilter;
import com.example.myapplication.insta.IFNormalFilter;
import com.example.myapplication.insta.IFRiseFilter;
import com.example.myapplication.insta.IFSierraFilter;
import com.example.myapplication.insta.IFSlumber;
import com.example.myapplication.insta.IFSutroFilter;
import com.example.myapplication.insta.IFToasterFilter;
import com.example.myapplication.insta.IFValenciaFilter;
import com.example.myapplication.insta.IFWaldenFilter;
import com.example.myapplication.insta.IFXprollFilter;
import com.example.myapplication.insta.InstaFilter;

import jp.co.cyberagent.android.gpuimage.filter.GPUImageFilter;


public class FilterHelper extends GPUImageFilter {

    private FilterHelper() {}

    private static final int FILTER_NUM = 21;
    private static IFImageFilter[] filters;

    public static IFImageFilter getFilter(Context context, int index) {
        if (filters == null) {
            filters = new IFImageFilter[FILTER_NUM];
        }
        try {
            switch (index){
                case 0:
                    filters[index] = new IFNormalFilter(context);
                    break;
                case 1:
                    filters[index] = new IFAmaroFilter(context);
                    break;
                case 2:
                    filters[index] = new IFRiseFilter(context);
                    break;
                case 3:
                    filters[index] = new IFHudsonFilter(context);
                    break;
                case 4:
                    filters[index] = new IFXprollFilter(context);
                    break;
                case 5:
                    filters[index] = new IFSierraFilter(context);
                    break;
                case 6:
                    filters[index] = new IFLomoFilter(context);
                    break;
                case 7:
                    filters[index] = new IFEarlybirdFilter(context);
                    break;
                case 8:
                    filters[index] = new IFSutroFilter(context);
                    break;
                case 9:
                    filters[index] = new IFToasterFilter(context);
                    break;
                case 10:
                    filters[index] = new IFBrannanFilter(context);
                    break;
                case 11:
                    filters[index] = new IFInkwellFilter(context);
                    break;
                case 12:
                    filters[index] = new IFWaldenFilter(context);
                    break;
                case 13:
                    filters[index] = new IFHefeFilter(context);
                    break;
                case 14:
                    filters[index] = new IFValenciaFilter(context);
                    break;
                case 15:
                    filters[index] = new IFNashvilleFilter(context);
                    break;
                case 16:
                    filters[index] = new IF1977Filter(context);
                    break;
                case 17:
                    filters[index] = new IFLordKelvinFilter(context);
                    break;
                case 18:
                    filters[index] = new IFLudwig(context);
                    break;
                case 19:
                    filters[index] = new IFSlumber(context);
                    break;
                case 20:
                    filters[index] = new IFAden(context);
                    break;
            }
        } catch (Throwable e) {
        }
        return filters[index];
    }

    public static void destroyFilters() {
        if (filters != null) {
            for (int i = 0; i < filters.length; i++) {
                try {
                    if (filters[i] != null) {
                        filters[i].destroy();
                        filters[i] = null;
                    }
                } catch (Throwable e) {
                }
            }
        }
    }

}
